var fetch = require('node-fetch');
var Form = require('form-data');

// var access = (code) => {
//     return {
//         gitlab: {
//             url: 'https://gitlab.com/oauth/token?client_id=3dea71e60ed478864d409ca1aee82d5ecd4f4234dc3621d46e15d240beaa9faa&client_secret=f3b5b9f7c84f591c8ef9cf65f32007d5cfe2926e8d19b411b92db76bb85dc0e6&code=' + code + '&grant_type=authorization_code&redirect_uri=http://localhost:3000/gitlab',
//             options: {method: 'POST'}
//         },
//         yammer: {
//             url: 'https://www.yammer.com/oauth2/access_token?client_id=cl7coSDecOhvh08AkWKg&client_secret=NYiRPmCga3jdSvTQaX71fhGLH82op6JGwExGzfmH8k&code=' + code + '&grant_type=authorization_code',
//             options: {method: 'POST'}
//         }
//     }
// }

// var infos = (access_token) => {
//     return {
//         gitlab: {
//             url: 'https://gitlab.com/api/v4/user?access_token=' + access_token,
//             options: {method: 'GET'}
//         }
//     }
// }

module.exports = async function (code) {
    let username = String;
    let access_token = String;
    let urlAccess = 'https://www.yammer.com/oauth2/access_token?client_id=cl7coSDecOhvh08AkWKg&client_secret=NYiRPmCga3jdSvTQaX71fhGLH82op6JGwExGzfmH8k&code=' + code + '&grant_type=authorization_code';
    console.log('RESQUEST ACCESS TOKEN IN YAMMER', urlAccess);
    await fetch(urlAccess, {method: 'POST'}).then(response => response.text()).then(async res => {
        console.log('RES', res);
        let parsed = JSON.parse(res);
        access_token = parsed.access_token.token;
        username = parsed.user.full_name;
    });
    return ({username: username, access_token: access_token, refresh_token: "no need :-)"});
}