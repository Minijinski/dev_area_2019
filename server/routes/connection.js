var express = require('express');
var router = express.Router();
var User = require('../schema/schema_user');
var Form = require('form-data');
var fetch = require('node-fetch');
var mongoose = require('mongoose');
var findService = require('../services/infoServices');

router.post('/add_service', async (req, res) => {
    let id = req.body.id;
    var info;
    var hasfound = false;
    var already_exist = false;
    let updatedUser = await User.findOne({id: id});
    if (!id || !updatedUser || !req.body.name || !req.body.code) {
        return res.status(404).send("ERROR 400 Missing argument in body need [id, name, code]");
    }
    if (req.body.code !== -42) {
        info = await findService(req.body.name, req.body.code);
    } else {
        info = {
            access_token: req.body.access_token,
            refresh_token: req.body.refresh_token,
            username: req.body.username
        };
    }
    if (!info)
        return res.status(400).send('ERROR 400 Missing service or incorrect service Name.');
    console.log('UPDATE', updatedUser.connections);
    updatedUser.connections.map(elem => {
        if (elem.name === req.body.name) {
            elem.info.map(map_info => {
                if (map_info.username === info.username) {
                    map_info = info;
                    already_exist = true;
                    return map_info;
                }
            })
            if (already_exist === false)
                elem.info.push(info);
            hasfound = true;
        }
        return elem;
    });
    if (hasfound === true) {
        await updatedUser.save();
        return res.status(200).json('200');
    }
    connection = {
        name: req.body.name,
        info: []
    };
    connection.info.push(info);
    updatedUser.connections.push(connection);
    await updatedUser.save();
    res.status(200).json('200');
});

router.get('/:id/:service', async (req, res) => {
    let id = req.params.id;
    let serviceName = req.params.service;
    try {
        let user = await User.findOne({id: id});
        for (let i = 0; i < user.connections.length; i++) {
            const elem = user.connections[i];
            if (elem.name === serviceName) {
                return res.status(200).json(elem);
            }
        }
        return res.status(200).json({info: []});
    } catch (err) {
        console.error('Got an error when trying to find user by id:', id, 'for the service', serviceName);
        console.error(err);
        return res.status(404).send("Error 404, User or Service not found");
    }
});

module.exports = router