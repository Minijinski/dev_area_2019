var express = require('express');
var router = express.Router();
var User = require('../schema/schema_user');

router.delete('/delete/:id', async(req, res, next) => {
    let id = req.params.id;
    if (!User.findOne({id: id}))
        res.status(400).send("User not found.");
    else {
        await User.deleteOne({id: id});
        res.status(200).send("User " + id + " deleted succesfully.");
    }
})

router.delete('/automation/delete/:id', async(req, res, next) => {
    let user = req.params.id;
    let auto_index = req.query.auto_id;
    if (!user || !auto_index)
        return res.status(300).send("either user id or auto_index is missing.");
    updatedUser = await User.findOne({id: user});
    if (!updatedUser)
        return res.status(400).send("User not found.");
    else {
        updatedUser.automation.splice(auto_index, 1);
        await updatedUser.save();
        res.status(200).json("Automation " + auto_index + " has been successfully deleted.");
    }
    return;
})

router.patch('/automation/update/:id', async(req, res, next) => {
    let id = req.params.id;
    let index = req.body.index;
    let automation = req.body.auto;
    let updatedUser = await User.findOne({id: id});

    if (!updatedUser)
        return res.send("USER NOT FOUND");
    parsed_auto = JSON.parse(automation);
    if (!parsed_auto)
        return res.status(400).send("Error in query");
    updatedUser.automation[index] = parsed_auto.automation;
    await updatedUser.save();
    res.status(200).json(updatedUser);
})

router.post('/automation/:id', async(req, res, next) => {
    let id = req.params.id;
    let automation = req.body.auto;
    let updatedUser = await User.findOne({id: id});

    if (!updatedUser)
        return res.send("USER NOT FOUND");
    parsed_auto = JSON.parse(automation);
    if (parsed_auto.automation.action)
        parsed_auto.automation.action.data = JSON.stringify(parsed_auto.automation.action.data);
    if (parsed_auto.automation.reaction)
        parsed_auto.automation.reaction.data = JSON.stringify(parsed_auto.automation.reaction.data);
    if (!parsed_auto)
        return res.status(400).send("Error in query");
    updatedUser.automation.push(parsed_auto.automation);
    await updatedUser.save();
    res.status(200).json(updatedUser);
})

router.get('/:id', async(req, res, next) => {
    var id = req.params.id;
    const user = await User.findOne({id: id});
    if (!user)
        res.status(400).send("User not found.");
    else
        res.status(200).json(user);
})

module.exports = router;