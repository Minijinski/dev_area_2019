var express = require('express');
var router = express.Router();
var User = require('../schema/schema_user');
var mongoose = require('mongoose');

/* GET users listing. */
router.post('/', async(req, res, next) => {
  const user = {
    id: req.body.id,
    username: req.body.username,
  };
  if (!user.id || !user.username) {
      res.status(300).send('Missing an argument in body');
      return;
  }
  try {
    const newUser = new User(user);
    await newUser.save();
    res.status(200).json(newUser);
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
  return;
})

module.exports = router;
