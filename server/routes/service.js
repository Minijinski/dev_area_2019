var express = require('express');
var router = express.Router();

router.get('/', async (req, res) => {
    all_services = [
        {
            name: "Imgur",
            connection_needed: true,
            actions: [{name: "When a new image is uploaded...", id: "postImage"}, {name: "When a new image has been add to favorites...", id: "favImage"}, {name: "When a new commentary has been posted...", id: "newCom"}],
            reactions: []
        },
        {
            name: "Spotify",
            connection_needed: true,
            actions: [],
            reactions: [{name: "Play the next track on the queue", id: "nextTrack"}, {name: "Pause or play the current track", id: "pauseTrack"}]
        },
        {
            name: "Steam",
            connection_needed: true,
            actions: [{name: "Last time the user has lag off...", id: "lastLogOff"}],
            reactions: [] 
        },
        {
            name: "Gitlab",
            connection_needed: true,
            actions: [{name: "When a commit has been send on the branch Master...", id: "updateCommit", fields: ["Path to the project (Username/name_of_the_project)"]}],
            reactions: []
        },
        {
            name: "Weather",
            connection_needed: false,
            actions: [{name: "When the temperature has reach a threshold...", id: "temperatureUpdate", fields: ["Name of the city", "suporinf", "Temperature in celsius"]}, {name: "When the speed has reach a threshold...", id: "windSpeed", fields: ["Name of the city", "suporinf", "Speed of the wind in m/s"]}],
            reactions: []
        },
        {
            name: "Microsoft",
            connection_needed: true,
            actions: [{name: "When you receive a mail...", id: "newMail"}],
            reactions: []
        },
        {
            name: "Facebook",
            connection_needed: true,
            actions: [{name: "The number of friends have changed...", id: "number_of_friends"}, {name: "The user made a new post...", id: "get_last_post_timestamp"}, {name: "When the user has been tagged on a picture...", id: "get_last_tagged_picture"}, {name: "When the user upload a new picture...", id: "get_last_uploaded_picture"}],
            reactions: []
        },

    ];
    res.status(200).json(all_services);
});

module.exports = router