FROM node:carbon
WORKDIR /app
COPY ./shared ./shared
COPY ./web ./web
WORKDIR /app/shared
RUN npm install
WORKDIR /app/web
RUN npm install
EXPOSE 4242
CMD npm run develop -- -p 4242 -H "0.0.0.0"