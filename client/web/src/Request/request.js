import React from 'react';
import axios from 'axios'

// export default class Request extends React.Component {

    export const addNewUser = async (profile) => {
        console.log("AAAAAA", profile);
        const response = await axios.post(
            'http://10.106.1.246:3000/register',
            { id: profile.sub,
            username: profile.name },
            { headers: { 'Content-Type': 'application/json' } }
        )
        console.log(response.data)

        // const response = await axios.delete(
        //     'http://10.106.1.246:3000/user/delete/5',
        //     { headers: { 'Content-Type': 'application/json' } }
        // )
        // console.log(response.data)

        // console.log("me");
        // const response =
        //     await axios.get("http://10.106.1.246:3000/register")
        // console.log(response.data)
    }

    export const getAccounts = async (serviceName) => {
        var userId = localStorage.getItem("user_id");
        const response =
            await axios.get("http://10.106.1.246:3000/connection/" + userId + "/" + serviceName)
        // console.log(response.data);
        return (response.data);
    }

    export const getServices = async () => {
        const response =
            await axios.get("http://10.106.1.246:3000/service")
        // console.log(response.data);
        return (response.data);
    }

    export const getImgurAccess = async (values) => {
        const response = await axios.post(
            'http://10.106.1.246:3000/connection/add_service',
            { id: localStorage.getItem("user_id"),
                name: "Imgur",
                code: -42,
                access_token: values.access_token,
                refresh_token: values.refresh_token,
                username: values.account_username
            }
        )
        handleResponse(response);
    }

    export const getSpotifyAccess = async (_code) => {
        const response = await axios.post(
            'http://10.106.1.246:3000/connection/add_service',
            { id: localStorage.getItem("user_id"),
                name: "Spotify",
                code: _code,
            }
        )
        handleResponse(response);
    }

    export const getGitlabAccess = async (_code) => {
        console.log(localStorage.getItem("user_id"));
        const response = await axios.post(
            'http://10.106.1.246:3000/connection/add_service',
            { id: localStorage.getItem("user_id"),
            name: "Gitlab",
            code: _code,
            }
        )
        handleResponse(response);
    }

    export const getMicrosoftAccess = async (_code) => {
        const response = await axios.post(
            'http://10.106.1.246:3000/connection/add_service',
            { id: localStorage.getItem("user_id"),
            name: "Microsoft",
            code: _code,
            }
        )
        handleResponse(response);
    }

    const handleResponse = (response) => {
        if (response.data == "200") {
            localStorage.setItem("isRedirectionOver", "true");
            window.close();
        } else if (response.data == "404") {
            console.log(response.data);
            window.close();
        }
    }
