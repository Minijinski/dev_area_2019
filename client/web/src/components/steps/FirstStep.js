import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import { getServices } from "../../Request/request";

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        margin: "25px",
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

function FirstStep(props) {

    const classes = useStyles();
    const [firstService, setFirstService] = React.useState(props.stateNewAutomation.firstService.name);
    const [secondService, setSecondService] = React.useState(props.stateNewAutomation.secondService.name);

    const [listActions, setListActions] = React.useState([]);
    const [listReactions, setListReactions] = React.useState([]);

    const updateServices = () => {
        getServices().then(res => {
            let noneElem = {
                name: "None",
            }
            // res.push(noneElem);
            console.log(res);
            res.map(elem => {
                if (elem.actions.length != 0)
                    setListActions(listActions => [...listActions, elem])
                if (elem.reactions.length != 0)
                    setListReactions(listReactions => [...listReactions, elem])
            })
            setListActions(listActions => [...listActions, noneElem])
            setListReactions(listReactions => [...listReactions, noneElem])
            console.log(res);
        })
    }

    useEffect(() => {
        updateServices();
    }, []);

    useEffect(() => {
        console.log(listActions);
    }, [listActions])

    const setConnection = () => {
        var fService = listActions.filter(elem => {
            // console.log(elem.name === props.stateNewAutomation.secondService);
            return elem.name === firstService
        });

        var sService = listReactions.filter(elem => {
            // console.log(elem.name === props.stateNewAutomation.secondService);
            return elem.name === secondService
        });

        var firstBool = false;
        var secondBool = false;

        if (fService[0] != undefined && sService[0] != undefined) {
            firstBool = fService[0].connection_needed;
            secondBool = sService[0].connection_needed;
            // console.log(fService[0].name, ": ", firstBool);
            // console.log(sService[0].name, ": ", secondBool);
        }
        return [firstBool, secondBool]
    }

    useEffect(() => {
        // console.log("List actions: ", listActions);
        // console.log("List Reactions: ", listReactions);
        if (firstService != "None" && secondService != "None") {
            var bools = setConnection();
            props.dispatch({
                type: "UPDATE_SERVICES",
                services: {
                    firstService: firstService,
                    secondService: secondService,
                    firstConnection: bools[0],
                    secondConnection: bools[1],
                    firstAccount: "None",
                    secondAccount: "None"
                }
            });
            props.dispatch({
                type: "UPDATE_NEXT_BUTTON",
                value: true
            });
        } else {
            props.dispatch({
                type: "UPDATE_NEXT_BUTTON",
                value: false
            });
        }
    })


    const handleFirstService = event => {
        setFirstService(event.target.value);
        // console.log(listServices);
    };

    const handleSecondService = event => {
        setSecondService(event.target.value);
    };

    return (
        <div>
            <FormControl className={classes.formControl}>
                <p>Connect this...</p>
                {/* <InputLabel id="demo-simple-select-label"></InputLabel> */}
                <Select
                    labelId="simple-select-label"
                    id="simple-select"
                    value={firstService}
                    onChange={handleFirstService}
                >
                    {/* <MenuItem value="oui">oui</MenuItem> */}
                    {listActions.map(elem => (
                        <MenuItem key={elem.name} value={elem.name}>{elem.name}</MenuItem>
                    ))}
                </Select>
            </FormControl>

            <FormControl className={classes.formControl}>
                <p>with this !</p>
                {/* <InputLabel id="demo-simple-select-label"></InputLabel> */}
                <Select
                    labelId="simple-select-label"
                    id="simple-select"
                    value={secondService}
                    onChange={handleSecondService}
                >
                    {/* <MenuItem value="non">non</MenuItem> */}
                    {listReactions.map(elem => (
                        <MenuItem key={elem.name} value={elem.name}>{elem.name}</MenuItem>
                    ))}
                </Select>
            </FormControl>

        </div>
    )
}

function mapStateToProps(state) {
    return {
        gridAutomations: state.gridAutomations,
        display: state.display,
        stateNewAutomation: state.stateNewAutomation,
        isNextEnabled: state.isNextEnabled,
    };
}

export default connect(mapStateToProps)(FirstStep);