import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import { getServices } from "../../Request/request";

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        margin: "25px"
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

function ThirdStep(props) {

    const classes = useStyles();
    const [listServices, setListServices] = React.useState([]);

    const [action, setAction] = React.useState(props.stateNewAutomation.action);
    const [reaction, setReaction] = React.useState(props.stateNewAutomation.reaction);

    const [listAction, setListAction] = React.useState([]);
    const [listReaction, setListReaction] = React.useState([]);

    useEffect(() => {
        getServices().then(res => {
            setListServices(res);
        })

    }, []);

    useEffect(() => {
        let noneElem = {
            name: "None",
            id: "404"
        }

        var newListActions = listServices.filter(elem => {
            // console.log(elem.name === props.stateNewAutomation.secondService);
            return elem.name === props.stateNewAutomation.firstService.name
        });
        if (newListActions[0] != undefined) {
            newListActions[0].actions.push(noneElem);
            // console.log(newListActions[0].actions);
            setListAction(newListActions[0].actions);
        }

        var newListReactions = listServices.filter(elem => {
            // console.log(elem.name === props.stateNewAutomation.secondService);
            return elem.name === props.stateNewAutomation.secondService.name
        });
        if (newListReactions[0] != undefined) {
            newListReactions[0].reactions.push(noneElem);
            // console.log(newListReactions[0]);
            setListReaction(newListReactions[0].reactions);
        }

    }, [listServices])

    useEffect(() => {
        if (action != "None" && reaction != "None") {
            props.dispatch({
                type: "UPDATE_AREA",
                obj: {
                    action: action,
                    reaction: reaction
                }
            });
            props.dispatch({
                type: "UPDATE_NEXT_BUTTON",
                value: true
            })
        } else {
            props.dispatch({
                type: "UPDATE_NEXT_BUTTON",
                value: false
            })
        }
    })

    const handleAction = event => {
        // console.log("Result action", listAction);
        // console.log("Result reaction", listReaction);
        setAction(event.target.value);
    }

    const handleReaction = event => {
        setReaction(event.target.value);
    }

    return (
        <div>
            <FormControl className={classes.formControl}>
                <p>Choose your action with {props.stateNewAutomation.firstService.name}...</p>
                <Select
                    labelId="simple-select-label"
                    id="simple-select"
                    value={action}
                    onChange={handleAction}
                >
                    {/* <MenuItem value="oui">oui</MenuItem> */}
                    {listAction.map(elem => (
                        <MenuItem key={elem.id} value={elem.name}>{elem.name}</MenuItem>
                    ))}
                </Select>
            </FormControl>

            <FormControl className={classes.formControl}>
                <p>and your reaction with {props.stateNewAutomation.secondService.name}</p>
                <Select
                    labelId="simple-select-label"
                    id="simple-select"
                    value={reaction}
                    onChange={handleReaction}
                >
                    {/* <MenuItem value="non">non</MenuItem> */}
                    {listReaction.map(elem => (
                        <MenuItem key={elem.id} value={elem.name}>{elem.name}</MenuItem>
                    ))}
                </Select>
            </FormControl>

        </div>
    )
}

function mapStateToProps(state) {
    return {
        gridAutomations: state.gridAutomations,
        display: state.display,
        stateNewAutomation: state.stateNewAutomation,
        isNextEnabled: state.isNextEnabled
    };
}

export default connect(mapStateToProps)(ThirdStep);