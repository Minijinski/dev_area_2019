import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import { getAccounts } from "../../Request/request";
import oauth2 from "../../oauth2"

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        margin: "25px",
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    buttons: {
        marginTop: "20px"
    }
}));

function SecondStep(props) {

    const classes = useStyles();
    const [firstAccount, setFirstAccount] = React.useState(props.stateNewAutomation.firstAccount);
    const [secondAccount, setSecondAccount] = React.useState(props.stateNewAutomation.secondAccount);
    const [firstListAccounts, setFirstListAccounts] = React.useState([]);
    const [secondListAccounts, setSecondListAccounts] = React.useState([]);

    var isFirstConnectionNeeded = props.stateNewAutomation.firstService.isConnectionNeeded;
    var isSecondConnectionNeeded = props.stateNewAutomation.secondService.isConnectionNeeded;

    const updateAccounts = () => {
        getAccounts(props.stateNewAutomation.firstService.name).then(res => {
            let noneElem = {
                username: "None"
            };
            res.info.push(noneElem);
            // console.log(res.info);
            setFirstListAccounts(res.info);
        })

        getAccounts(props.stateNewAutomation.secondService.name).then(res => {
            let noneElem = {
                username: "None",
            }
            res.info.push(noneElem);
            setSecondListAccounts(res.info);
        })
    }

    useEffect(() => {
        updateAccounts();
        localStorage.setItem("isRedirectionOver", "false");
    }, [])

    const setNext = (bool) => {
        props.dispatch({
            type: "UPDATE_NEXT_BUTTON",
            value: bool
        })
    }

    useEffect(() => {
        // console.log(firstListAccounts, secondListAccounts);
        if (firstAccount != "None" && secondAccount != "None") {
            props.dispatch({
                type: "UPDATE_ACCOUNT",
                accounts: {
                    firstAccount: firstAccount,
                    secondAccount: secondAccount
                }
            });
            setNext(true);
        } else
            setNext(false);

        if ((firstAccount != "None" && isSecondConnectionNeeded == false) ||
            (secondAccount != "None" && isFirstConnectionNeeded == false)) {
            props.dispatch({
                type: "UPDATE_NEXT_BUTTON",
                value: true
            })
        }
    });

    const buttonClicked = () => {
        // console.log(localStorage.getItem("isRedirectionOver"));
        if (localStorage.getItem("isRedirectionOver") == "true") {
            console.log("Update Account");
            updateAccounts();
            // localStorage.setItem("isRedirectionOver", "false");
        }
    }

    const handleFirstAccount = event => {
        setFirstAccount(event.target.value);
    };

    const handleSecondAccount = event => {
        setSecondAccount(event.target.value);
    };

    const addAccountFirstService = () => {
        // console.log(props.stateNewAutomation.firstService);
        switch (props.stateNewAutomation.firstService.name) {
            case "Gitlab":
                window.open(oauth2.gitlab, "_blank");
                break;
            case "Microsoft":
                window.open(oauth2.microsoft, "_blank");
                break;
            case "Imgur":
                window.open(oauth2.imgur, "_blank");
                break;
            case "Spotify":
                window.open(oauth2.spotify, "_blank");
                break;
            default:
                break;
        }
    }

    const addAccountSecondService = () => {
        // console.log(props.stateNewAutomation.secondService);
        switch (props.stateNewAutomation.secondService.name) {
            case "Gitlab":
                window.open(oauth2.gitlab, "_blank");
                break;
            case "Microsoft":
                window.open(oauth2.microsoft, "_blank");
                break;
            case "Imgur":
                window.open(oauth2.imgur, "_blank");
                break;
            case "Spotify":
                window.open(oauth2.spotify, "_blank");
                break;    
            default:
                break;
        }
    }

    return (
        <div>
            <FormControl className={classes.formControl}>
                <p>Choose {props.stateNewAutomation.firstService.name} account</p>
                {/* <InputLabel id="demo-simple-select-label"></InputLabel> */}
                <Select
                    labelId="simple-select-label"
                    id="simple-select"
                    value={firstAccount}
                    onChange={handleFirstAccount}
                    onOpen={buttonClicked}
                >
                    {/* <MenuItem value="oui">oui</MenuItem> */}
                    {firstListAccounts.map(elem => (
                        <MenuItem key={elem.name} value={elem.username}>{elem.username}</MenuItem>
                    ))}
                </Select>
                <Button
                    disabled={!isFirstConnectionNeeded}
                    variant="contained"
                    color="primary"
                    onClick={addAccountFirstService}
                    className={classes.buttons}
                    >
                    Add Account
                </Button>
            </FormControl>

            <FormControl className={classes.formControl}>
                <p>Choose {props.stateNewAutomation.secondService.name} account</p>
                {/* <InputLabel id="demo-simple-select-label"></InputLabel> */}
                <Select
                    labelId="simple-select-label"
                    id="simple-select"
                    value={secondAccount}
                    onChange={handleSecondAccount}
                    onOpen={buttonClicked}
                >
                    {/* <MenuItem value="non">non</MenuItem> */}
                    {secondListAccounts.map(elem => (
                        <MenuItem key={elem.name} value={elem.username}>{elem.username}</MenuItem>
                    ))}
                </Select>
                <Button
                    disabled={!isSecondConnectionNeeded}
                    variant="contained"
                    color="primary"
                    onClick={addAccountSecondService}
                    className={classes.buttons}
                    >
                    Add Account
                </Button>
            </FormControl>

        </div>
    )
}

function mapStateToProps(state) {
    return {
        gridAutomations: state.gridAutomations,
        display: state.display,
        stateNewAutomation: state.stateNewAutomation,
        isNextEnabled: state.isNextEnabled,
    };
}

export default connect(mapStateToProps)(SecondStep);