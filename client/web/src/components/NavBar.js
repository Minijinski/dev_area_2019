import React from "react";
import { Toolbar, AppBar, IconButton, Typography, Grid } from "@material-ui/core";
import { AccountCircle } from '@material-ui/icons';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { makeStyles } from '@material-ui/core/styles';
// import history from '../History';
import MenuManager from './StyledMenu';
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";
import { contentState } from "./Grid"
import { connect } from 'react-redux';
import Dialog from "./Dialog";

const useStyles = makeStyles(theme => ({
    fullWide: {
        width: '100%',
    },
    semiFullWide: {
        width: '93%',
    },
    fonts: {
        fontSize: 12,
    },
    button: {
        color: 'white',
        margin: theme.spacing(1),
    },
    input: {
        display: 'none',
    },
    padding: {
        minHeight: 94,
    },
    marginBot: {
        marginBottom: '30px',
    }
}));

function NavBar(props) {
    var isAuthenticated = true;
    const [anchorEl, setAnchorEl] = React.useState(null);
    // var isAuthenticated = props.isAuthenticated();
    const classes = useStyles();
    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleMenu = event => {
        setAnchorEl(event.currentTarget);
    }
    const login = () => {
        // props.login();
        // isAuthenticated = props.isAuthenticated();
    }

    const logout = () => {
        props.logout();
        isAuthenticated = props.isAuthenticated();
    }

    const Dashboard = () => {
        // history.replace('/dashboard');
    }

    const Settings = () => {
        // history.replace('/settings');
    }

    const backToHome = () => {
        // console.log("DISPLAY", props);
        // if (props.display == contentState.NEW_AUTOMATION.FIRST_STEP ||
        //     props.display == contentState.NEW_AUTOMATION.SECOND_STEP ||
        //     props.display == contentState.NEW_AUTOMATION.THIRD_STEP) {
        //         return <Dialog></Dialog>
        //     }
        // else {
            // props.dispatch({
            //     type: "CHANGE_STATE",
            //     newDisplay: contentState.GRID
            // });
        // }
    }

    return (
        <React.Fragment>
            <AppBar position="static" className={classes.marginBot}>
                <Toolbar className={classes.padding}>
                    <Grid
                        justify="flex-start"
                        container
                        alignItems="center"
                    >
                        <Grid item onClick={() => backToHome()}>
                            <Dialog value="My services"></Dialog>
                        </Grid>
                    </Grid>

                    <Grid
                        direction="row"
                        justify="flex-end"
                        container 
                        alignItems="center"
                    >
                    {(isAuthenticated) && (
                        <div>
                        <IconButton
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit">
                            <AccountCircle />
                        </IconButton>
                        <MenuManager
                            names={['Settings', 'Dashboard', 'Log out']}
                            actions={[Settings, Dashboard, logout]}
                            icons={[<SettingsIcon fontSize="small" />,
                                    <DashboardIcon fontSize="small" />,
                                    <ExitToAppIcon fontSize="small" />]}
                            anchorEl={anchorEl}
                            setAnchorEl={setAnchorEl}
                            handleClose={handleClose}/>
                        </div>
                        )}
                    </Grid>
                </Toolbar>
            </AppBar>
        </React.Fragment>
    );
};

function mapStateToProps(state) {
    return {
        gridAutomations: state.gridAutomations,
        display: state.display,
        stateAutomation: state.stateAutomation
    };
}

export default connect(mapStateToProps)(NavBar);