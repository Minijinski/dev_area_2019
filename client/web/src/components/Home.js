import React, { Component } from 'react';

export default class Home extends Component {
    componentDidMount() {
        if (!this.props.auth.isAuthenticated())
            this.props.auth.login();
    }

    render() {
    // calls the isAuthenticated method in authentication service
        const { isAuthenticated } = this.props.auth;
        return (
            <div>
            {
                isAuthenticated() &&
                <div>
                    <div className="App">
                        <header className="App-header">
                        {/* <img src={logo} className="App-logo" alt="logo" /> */}
                        <p>
                            Edit <code>src/App.js</code> and save to reload.
                        </p>
                        <a
                            className="App-link"
                            href="https://reactjs.org"
                            target="_blank"
                            rel="noopener noreferrer"
                        >Learn React
                        </a>
                        </header>
                    </div>
                </div>
            }
            </div>
        );
    }
}