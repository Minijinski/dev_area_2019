import React, { Component } from 'react';
import Grid from "../components/Grid";
import { makeStyles } from '@material-ui/core/styles';
import { getServices } from "../Request/request"
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    content: {
        paddingLeft: "24px",
        paddingRight: "24px"
    }
}));

export default class Content extends Component {

    componentDidMount() {
        if (!this.props.auth.isAuthenticated()) {
            console.log("wtf", this.props);
            this.props.auth.login();
        }
    }

    gitlabClicked() {
        // getServices();
        window.location = "https://gitlab.com/oauth/authorize?client_id=3dea71e60ed478864d409ca1aee82d5ecd4f4234dc3621d46e15d240beaa9faa&redirect_uri=http://localhost:3000/gitlab&response_type=code&scope=api+read_user+read_repository+write_repository+read_registry+openid+profile+email"
    }

    displayApp = () => {
        const classes = useStyles();
        return (
            <div className={classes.content}>
                <Grid />
                <Button onClick={() => this.gitlabClicked()}>GITLAB</Button>
            </div>
        )
    }

    render() {
        const { isAuthenticated } = this.props.auth;
        return (
        <div>
        {
            isAuthenticated() &&
            <this.displayApp></this.displayApp>
        }
        </div>
        );
    }
}