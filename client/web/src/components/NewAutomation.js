import React from 'react';
// import InputLabel from '@material-ui/core/InputLabel';
import { connect } from 'react-redux';
import { contentState } from './Grid';
import FirstStep from "./steps/FirstStep"
import SecondStep from "./steps/SecondStep"
import ThirdStep from "./steps/ThirdStep"
import Stepper from './Stepper';
import { makeStyles } from '@material-ui/core/styles';

function NewAutomation(props) {

    // console.log("Props", props);
    const useStyles = makeStyles(theme => ({
        parent: {
            textAlign: "center",
            width: "70%",
            margin: "auto"
        },
        children: {
            margin: "auto"
        }
    }));

    const classes = useStyles();

    const renderSwitch = (step) => {
        switch (step.display) {
            case contentState.NEW_AUTOMATION.FIRST_STEP:
                return <FirstStep></FirstStep>
            case contentState.NEW_AUTOMATION.SECOND_STEP:
                return <SecondStep></SecondStep>;
            case contentState.NEW_AUTOMATION.THIRD_STEP:
                return <ThirdStep></ThirdStep>
        }
    }

    return (
        <div>
            <div className={classes.parent}>
                <Stepper></Stepper>
                <div className={classes.children}>
                    {renderSwitch(props)}
                </div>
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        gridAutomations: state.gridAutomations,
        display: state.display,
        stateNewAutomation: state.stateNewAutomation,
        isNextEnabled: state.isNextEnabled,
    };
}

export default connect(mapStateToProps)(NewAutomation);