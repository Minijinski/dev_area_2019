import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { contentState } from './Grid';

const useStyles = makeStyles(theme => ({
    root: {
        paddingLeft: "0px",
        paddingRight: "0px",
        textAlign: "center",
        width: "80%",
        margin: "auto"
},
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    buttons: {
        paddingTop: "14px",
        paddingBot: "14px"
    }
}));

function getSteps() {
    return ['Select your services', 'Select your accounts', 'Create your action/reaction'];
}

function getStepContent(step) {
    switch (step) {
        case 0:
            return 'Select campaign settings...';
        case 1:
            return 'What is an ad group anyways?';
        case 2:
            return 'This is the bit I really care about!';
        default:
            return 'Unknown step';
    }
}

function HorizontalLinearStepper(props) {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    // const [skipped, setSkipped] = React.useState(new Set());
    const steps = getSteps();

    const handleNext = () => {

        if (activeStep === steps.length - 1) {
            props.dispatch({
                type: "CHANGE_STATE", 
                newDisplay: 0
            });
            console.log(props);
        } else {
            props.dispatch({
                type: "CHANGE_STATE", 
                newDisplay: props.display + 1,
            });
            setActiveStep(prevActiveStep => prevActiveStep + 1);
        }
    };

    const handleBack = () => {
        props.dispatch({
            type: "CHANGE_STATE", 
            newDisplay: props.display - 1,
        });

        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    return (
        <div>
            <Stepper className={classes.root} activeStep={activeStep}>
                {steps.map((label, index) => {
                    const stepProps = {};
                    const labelProps = {};
                    return (
                        <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>
            <div>
            {activeStep === steps.length ? (
                <div>
                    <Typography className={classes.instructions}>
                        All steps completed - you&apos;re finished
                    </Typography>
                    <Button onClick={handleReset} className={classes.button}>
                        Reset
                    </Button>
                </div>
            ) : (
                <div>
                    {/* <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography> */}
                    <div className={classes.buttons}>
                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                            Back
                        </Button>

                        <Button
                            disabled={!props.isNextEnabled}
                            variant="contained"
                            color="primary"
                            onClick={handleNext}
                            className={classes.button}
                            >
                            {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                        </Button>
                    </div>
                </div>
            )}
            </div>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        gridAutomations: state.gridAutomations,
        display: state.display,
        stateNewAutomation: state.stateNewAutomation,
        isNextEnabled: state.isNextEnabled
    };
}

export default connect(mapStateToProps)(HorizontalLinearStepper);