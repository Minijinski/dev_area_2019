import React, { Component } from 'react';
import Grid from "../components/Grid"
// import { Route } from 'react-router-dom';
// import { Switch } from '@material-ui/core';
import NavBar from "./NavBar"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Routes from '../Routes/Routes'
import Auth from '../Auth/auth';
import { createStore } from 'redux';
import { contentState } from '../components/Grid'
import { Provider } from 'react-redux';

const auth = new Auth();

const initialState = {
    gridAutomations: {
        automations: []
    },
    display: contentState.GRID,
    stateNewAutomation: {
        firstService: {
            name: "None",
            isConnectionNeeded: false
        },
        secondService: {
            name: "None",
            isConnectionNeeded: false
        },
        firstAccount: "None",
        secondAccount: "None",
        action: "None",
        reaction: "None"
    },
    isNextEnabled: false
};

function reducer(currentState = initialState, action) {
    switch (action.type) {
        case "CHANGE_STATE":
            return {
                gridAutomations: currentState.gridAutomations,
                display: action.newDisplay,
                stateNewAutomation: currentState.stateNewAutomation,
            }
        case "UPDATE_SERVICES":
            var newAutomation = currentState.stateNewAutomation;
            newAutomation.firstService.name = action.services.firstService;
            newAutomation.secondService.name = action.services.secondService;

            newAutomation.firstService.isConnectionNeeded = action.services.firstConnection;
            newAutomation.secondService.isConnectionNeeded = action.services.secondConnection;

            newAutomation.firstAccount = action.services.firstAccount;
            newAutomation.secondAccount = action.services.secondAccount;
            return {
                gridAutomations: currentState.gridAutomations,
                display: currentState.display,
                stateNewAutomation: newAutomation
            }
        case "UPDATE_ACCOUNT":
            var newAutomation = currentState.stateNewAutomation;
            newAutomation.firstAccount = action.accounts.firstAccount;
            newAutomation.secondAccount = action.accounts.secondAccount;
            return {
                gridAutomations: currentState.gridAutomations,
                display: currentState.display,
                stateNewAutomation: newAutomation
            }
        case "UPDATE_AREA":
            var newAutomation = currentState.stateNewAutomation;
            newAutomation.action = action.obj.action;
            newAutomation.reaction = action.obj.reaction;
            return {
                gridAutomations: currentState.gridAutomations,
                display: currentState.display,
                stateNewAutomation: newAutomation
            }
        case "UPDATE_NEXT_BUTTON":
            return {
                gridAutomations: currentState.gridAutomations,
                display: currentState.display,
                stateNewAutomation: currentState.stateNewAutomation,
                isNextEnabled: action.value,
            }
        // case "REFRESH_PAGE":
        //     console.log("Refresh the page ?");
        //     return {
        //         gridAutomations: currentState.gridAutomations,
        //         display: currentState.display,
        //         stateNewAutomation: currentState.stateNewAutomation,
        //         isNextEnabled: currentState.isNextEnabled,
        //     }

        // case "ADD_AUTOMATION":
        //     console.log('reducer', currentState, action);
        //     let newGridAutomation = currentState.state.gridAutomations;
        //     newGridAutomation.automations.push(action.item);
        //     return {
        //         state: {
        //             gridAutomations: newGridAutomation,
        //             display: currentState.display
        //         }
        // }
    }
    return currentState;
}

const store = createStore(reducer);

export default class App extends Component {

    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <Router>
                        <div>
                            <NavBar {...auth}/>
                        </div>
                        <Routes {...auth}/>
                        {/* <footer>
                            © {new Date().getFullYear()}, Built with React
                        </footer> */}
                    </Router>
                </div>
            </Provider>
        );
    }
}