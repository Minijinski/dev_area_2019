import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import GridItem from './GridItem';
import { connect } from 'react-redux';
import { getServices } from '../Request/request';
import Automation from './Automation'
import NewAutomation from './NewAutomation'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
        paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export const contentState = {
    GRID: 0,
    AUTOMATION: 1,
    NEW_AUTOMATION: {
        FIRST_STEP: 2,
        SECOND_STEP: 3,
        THIRD_STEP: 4
    }
}

class MyGrid extends Component {

    constructor(props) {
        super(props);
        console.log(this.props);
    }

    addAutomation() {
        this.props.dispatch({ type: "CHANGE_STATE", newDisplay: contentState.NEW_AUTOMATION.FIRST_STEP});

        // this.props.dispatch({
        //     type: "ADD_AUTOMATION",
        //     item: {
        //         id: (this.props.state.gridAutomations.automations.length + 1)
        //     }
        // })
    }

    automationClicked(id) {
        console.log("Just clicked on ", id);
        getServices();
        this.props.dispatch({ type: "CHANGE_STATE", newDisplay: contentState.AUTOMATION});
    }

    displayGridAutomations = () => {
        const classes = useStyles();

        const listElements = this.props.gridAutomations.automations.map((elem) => {
            return (
                <Grid item xs={3} onClick={() => this.automationClicked(elem.id)}>
                    <GridItem id={elem.id}></GridItem>
                </Grid>
            )
        })

        return (
            <>
                {listElements}
                <Grid item xs={3} onClick={() => this.addAutomation()}>
                    <Paper className={classes.paper}>+</Paper>
                </Grid>
            </>
        )
    }

    displayGrid = () => {
        const classes = useStyles();

        return (
            <div className={classes.root}>
                <Grid container spacing={3}>
                    <this.displayGridAutomations></this.displayGridAutomations>
                </Grid>
            </div>
        );
    }

    render() {
        let content = <this.displayGrid></this.displayGrid>;

        switch (this.props.display) {
            case contentState.GRID:
                return content = <this.displayGrid></this.displayGrid>
            case contentState.AUTOMATION:
                return content = <Automation></Automation>
            case contentState.NEW_AUTOMATION.FIRST_STEP:
            case contentState.NEW_AUTOMATION.SECOND_STEP:
            case contentState.NEW_AUTOMATION.THIRD_STEP:
                return content = <NewAutomation></NewAutomation>
        }

        return (
            <div className="SIIISI">
                {content}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        gridAutomations: state.gridAutomations,
        display: state.display,
        stateAutomation: state.stateAutomation,
        isNextEnabled: state.isNextEnabled,
		test: state.test
    };
}

export default connect(mapStateToProps)(MyGrid);