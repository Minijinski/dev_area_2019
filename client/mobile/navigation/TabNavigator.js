import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import TabBarIcon from '../components/TabBarIcon';
import ServicesScreen from '../screens/ServicesScreen';
import NewServiceScreen from '../screens/NewServiceScreen';
import ProfileScreen from '../screens/ProfileScreen';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const ServicesStack = createStackNavigator(
  {
    Services: ServicesScreen,
  },
  config
);

ServicesStack.navigationOptions = {
  tabBarLabel: 'Services',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-home${focused ? '' : '-outline'}`
          : 'md-home'
      }
    />
  ),
};

ServicesStack.path = '';

const NewServiceStack = createStackNavigator(
  {
    NewService: NewServiceScreen,
  },
  config
);

NewServiceStack.navigationOptions = {
  tabBarLabel: 'New Service',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-add-circle' : 'md-add-circle'} />
  ),
};

NewServiceStack.path = '';

const ProfileStack = createStackNavigator(
  {
    'Profile': ProfileScreen,
  },
  config
);

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'} />
  ),
};

ProfileStack.path = '';

const tabNavigator = createBottomTabNavigator({
  ServicesStack,
  NewServiceStack,
  ProfileStack,
});

tabNavigator.path = '';

export default tabNavigator;
