import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { AuthSession } from 'expo'

import TabNavigator from './TabNavigator';
import AuthLoading from '../screens/AuthLoading';
import NewServiceNav from '../screens/NewServiceNav';

export default createAppContainer(
  createSwitchNavigator({
    Auth: AuthLoading,
    Main: TabNavigator,
    New: NewServiceNav,
  })
);
