import React from 'react';
import { ScrollView, StyleSheet, Text, Button } from 'react-native';
import { Overlay } from 'react-native-elements';

export default class NewServiceScreen extends React.Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <Text>POPO</Text>
      <Button
        title="NewServiceNav"
        onPress={() => this.props.navigation.navigate('New')}
      />
      </ScrollView>
    );
  }
}

NewServiceScreen.navigationOptions = {
  title: 'New Service',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
