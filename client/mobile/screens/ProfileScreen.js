import React from 'react';
import { ScrollView, StyleSheet, Text, Button } from 'react-native';

export default class ProfileScreen extends React.Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <Text>PIPI</Text>
        <Button
          title="Log Out"
          onPress={()=> this.props.navigation.navigate('Auth')}
        />
    </ScrollView>
    );
  }
}

ProfileScreen.navigationOptions = {
  title: 'Profile',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
