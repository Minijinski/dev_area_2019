import React from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';

export default function ServicesScreen() {
  return (
    <ScrollView style={styles.container}>
        <Text>PIPI</Text>
    </ScrollView>
  );
}

ServicesScreen.navigationOptions = {
  title: 'Services',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
