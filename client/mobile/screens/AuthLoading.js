import React from 'react';
import { ScrollView, StyleSheet, Text, Button } from 'react-native';
import { Header } from 'react-native-elements';

import SharedExample from '../../shared/SharedExample';

export default class AuthLoading extends React.Component {
    render() {
        return (
            <ScrollView>
            <Header
                centerComponent={{ text: 'AREA', style: { color: '#000' } }}
                containerStyle={{
                    backgroundColor: '#FFFFFF',
                    justifyContent: 'space-around',
                }}
            />
            <Text>
                <SharedExample name="That's a mobile App"/>
            </Text>
            <Text>
                <SharedExample name="That's a mobile App"/>
            </Text>
            <Text>
                <SharedExample name="That's a Mobile App"/>
            </Text>
            <Button
                title="Log In"
                onPress={() => this.props.navigation.navigate('Main')}
            />
            
            <Button
                title="Sign Up"
                onPress={() => this.props.navigation.navigate('Main')}
            />
            </ScrollView>
        );
    }
}
