import React from 'react';
import { ScrollView, Text, Button } from 'react-native';
import { Header } from 'react-native-elements';

export default class NewServiceNav extends React.Component {
    render() {
        return (
            <ScrollView>
            <Header
                centerComponent={{ text: 'NEW', style: { color: '#000' } }}
                containerStyle={{
                    backgroundColor: '#FFFFFF',
                    justifyContent: 'space-around',
                }}
            />
            <Button
                title="Save"
                onPress={() => this.props.navigation.navigate('Main')}
            />
            <Button
                title="Cancel"
                onPress={() => this.props.navigation.navigate('Main')}
            />
            </ScrollView>
        );
    }
}